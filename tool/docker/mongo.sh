#!/bin/bash

sudo docker stop my-mongo
sudo docker rm my-mongo
sudo docker run \
	--name my-mongo \
	-p 27017:27017 \
	-v /srv/data/db:/data/db \
	-v /srv/data/configdb:/data/configdb \
	-d mongo 
