#!/bin/bash

sudo docker run -d \
	--name myjenkins \
	--restart always \
	-p 18080:8080 \
	-p 50000:50000 \
	-v /srv/jenkins:/var/jenkins_home \
	jenkins

