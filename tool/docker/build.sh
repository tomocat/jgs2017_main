#!/bin/bash

SRCDIR="app"
WORKDIR=`pwd`
BUILD_ARG=""
CONTAINER_NAME="myregistry:5000/myapp"

if [[ $HTTP_PROXY != "" ]]; then
	BUILD_ARG="--build-arg HTTP_PROXY=${HTTP_PROXY}"
fi

# キャッシュを効かすため先に関連モジュールをインストール
cd $SRCDIR
npm install
cd $WORKDIR

# コンテナのビルド
sudo docker build . -t $CONTAINER_NAME $BUILD_ARG
sudo docker push $CONTAINER_NAME
