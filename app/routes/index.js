var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'JGS2017 Sample',
                          user: process.env.URL_USER,
                          task: process.env.URL_TASK });
});

module.exports = router;
