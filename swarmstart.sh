#!/bin/bash

ADDRESS=192.168.56.200

#docker network create \
#	--driver overlay \
#	 --subnet 10.0.9.0/24 \
#	 my-multi-host-network

#docker volume create --name db
#docker volume create --name configdb

#	--mount "type=bind,src=/srv/data/db,dst=/data/db" \
#	--mount "type=bind,src=/srv/data/configdb,dst=/data/configdb" \

# mongo
docker service create --replicas 1 --publish 27017:27017 \
	--network my-multi-host-network \
	--mount "type=volume,src=db,dst=/data/db" \
	--mount "type=volume,src=configdb,dst=/data/configdb" \
	--name mongo \
	mongo

# user
docker service create --replicas 1 --publish 3001:3001 \
	--network my-multi-host-network \
	--env URL_MAIN="http://${ADDRESS}:3000" \
	--env URL_MONGO="mongodb://mongo:27017/test" \
	--name user \
	jgs2017/user

# task
docker service create --replicas 1 --publish 3002:3002 \
	--network my-multi-host-network \
	--env URL_MAIN="http://${ADDRESS}:3000" \
	--env URL_MONGO="mongodb://mongo:27017/test" \
	--name task \
	jgs2017/task

# main
docker service create --replicas 1 --publish 3000:3000 \
	--network my-multi-host-network \
	--env URL_USER="http://${ADDRESS}:3001" \
	--env URL_TASK="http://${ADDRESS}:3002" \
	--name main \
	jgs2017/main
